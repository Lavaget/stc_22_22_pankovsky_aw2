drop table if exists client;
drop table if exists drive;
drop table if exists car;


create table client
(
    id                 bigserial primary key,
    first_name         char(20) default 'DEFAULT_FIRSTNAME',
    last_name          char(20) default 'DEFAULT_LASTTNAME',
    number_telephone   integer check (number_telephone > 0),
    work_experiens     integer check (work_experiens >= 0 and work_experiens <= 60),
    age                integer check (age >= 0 and age <= 120)     not null,
    drivers_license    bool,
    category_of_rights char(100),
    rating             integer check (rating >= 0 and rating <= 5) not null
);


create table car
(
    id         serial primary key,
    model      char(250)                                              not null,
    color      char(200),
    number_car integer check (number_car >= 0 and number_car <= 9999) not null,
    id_driver  integer check (id_driver >= 0 and id_driver <= 9999)   not null
);


create table drive
(
    id          serial primary key,
    id_driver   integer check (id_driver >= 0 and id_driver <= 9999) not null,
    id_car      integer check (id_car >= 0 and id_car <= 9999)       not null,
    data_trip   timestamp,
    travel_time integer check (travel_time >= 0)                     not null,
    car_id integer not null,
    foreign key (car_id) references car(id)
);



