insert into client (first_name, last_name, number_telephone, work_experiens, age, drivers_license, category_of_rights,
                    rating)
values ('Андрей', 'Андреев', 925777, 9, 37, true, 'B', 4),
       ('Борис', 'Борисов', 925444, 0, 37, false, '-', 0),
       ('Владимир', 'Владимиров', 925333, 9, 27, true, 'B', 2),
       ('Георг', 'Георгиев', 925227, 8, 47, true, 'A, C', 2),
       ('Дмитрий', 'Думов', 925377, 7, 57, true, 'B, C', 3);

insert into car (model, color, number_car, id_driver)
values ('Aston_Martin', 'white', 1, 13),
       ('BMW', 'silver', 222, 113),
       ('Bugatti', 'red', 333, 313),
       ('Bentley', 'orange', 44, 441),
       ('Buick', 'brown', 5555, 5551);

insert into drive (id_driver, id_car, data_trip, travel_time, car_id)
values (111, 1111, '2022-01-01', 9, 1),
       (222, 2222, '2022-02-02', 8, 2),
       (333, 3333, '2022-03-03', 7, 3),
       (444, 4444, '2022-04-04', 6, 4),
       (555, 5555, '2022-05-05', 5, 1),
       (777, 7777, '2022-05-05', 4, 5);

